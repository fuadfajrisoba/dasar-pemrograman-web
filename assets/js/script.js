// SHOW NAV MENU
const navToggle = document.querySelector(".nav_toggle");
const navList = document.querySelector(".nav_list");

navToggle.addEventListener("click", showMenu);

function showMenu() {
    navToggle.classList.toggle("active");
    navList.classList.toggle("active");
}

// REMOVE NAV MENU
const navLink = document.querySelectorAll(".nav_link");

navLink.forEach(n => n.addEventListener("click", closeMenu));

function closeMenu() {
    navToggle.classList.remove("active");
    navList.classList.remove("active");
}

// MENU ACTIVE
const articles = document.querySelectorAll('article[id]')

window.addEventListener('scroll', scrollActive)

function scrollActive(){
    const scrollY = window.pageYOffset
    
    articles.forEach(current =>{
        const articleHeight = current.offsetHeight;
        const articleTop = current.offsetTop - 50;
        articleId = current.getAttribute('id')
        
        if(scrollY > articleTop && scrollY <= articleTop + articleHeight){
            document.querySelector('.nav_menu a[href*=' + articleId + ']').classList.add('active_link')
        }else{
            document.querySelector('.nav_menu a[href*=' + articleId + ']').classList.remove('active_link')
        }
    })
}

//CHANGE BACKGROUND HEADER
window.addEventListener('scroll', scrollHeader)

function scrollHeader(){
    const nav = document.getElementById('header')
    if(this.scrollY >= 100) nav.classList.add('scroll-header'); else nav.classList.remove('scroll-header')
}